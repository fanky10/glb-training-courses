# Root Folder
/<languageName>_<tecnology_name>
# Inside we will find:
# this contains the course 
index.html
# resources of the course
img/ 

# Common resources:
/resources
/course_template

When a new course is created, simply copy and paste the template and fill the img folder with corresponding resources.

